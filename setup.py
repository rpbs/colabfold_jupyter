from setuptools import setup, find_packages

version="0.0.1"

with open('README.rst', encoding='utf-8') as readme_file:
    readme = readme_file.read()

requirements = [
    'colabfold>=1.5.5',
    'jaxlib',
]

setup(
    name='colabfold_jupyter',
    version=version,
    description=(
        'Colabfold_jupyter is a Python package that provides a'
        'jupyter notebook API implementation.'
    ),
    long_description=readme,
    long_description_content_type='text/markdown',
    author='Samuel Murail, Julien Rey',
    author_email="samuel.murail@u-paris.fr, julien.rey@u-paris.fr",
    url='https://gitlab.rpbs.univ-paris-diderot.fr/rpbs/colabfold_jupyter',
    packages=['colabfold_jupyter'],
    package_dir={'colabfold_jupyter': 'src/colabfold_jupyter'},
    entry_points={'console_scripts': ['colabfold_jupyter = colabfold_jupyter.__main__:main']},
    include_package_data=True,
    python_requires='>=3.7',
    install_requires=requirements,
    license='GNUv2.0',
    zip_safe=False,
    package_data={
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python",
        "Topic :: Software Development",
    ],
    keywords=[
        "colabfold",
        "Python",
        "jupyter",
        "notebook",
    ],
)
