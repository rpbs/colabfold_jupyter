import os
import re
import hashlib
import logging
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import shutil

from Bio import BiopythonDeprecationWarning
warnings.simplefilter(action='ignore', category=BiopythonDeprecationWarning)

from pathlib import Path
from colabfold.download import download_alphafold_params, default_data_dir
from colabfold.utils import setup_logging
from colabfold.batch import get_queries, run, set_model_type
from colabfold.plot import plot_msa_v2
from colabfold.colabfold import plot_protein


from typing import Dict,Any
import colabfold
from alphafold.data import templates, pipeline
from alphafold.data.tools import hhsearch
import sys

def mk_template(
    a3m_lines: str, template_path: str, query_sequence: str
) -> Dict[str, Any]:

    python_path = "/".join(sys.executable.split("/")[:-1])
    template_featurizer = templates.HhsearchHitFeaturizer(
        mmcif_dir=template_path,
        max_template_date="2100-01-01",
        max_hits=20,
        kalign_binary_path=python_path+"/kalign",
        release_dates_path=None,
        obsolete_pdbs_path=None,
    )

    hhsearch_pdb70_runner = hhsearch.HHSearch(
        binary_path=python_path+"/hhsearch", databases=[f"{template_path}/pdb70"]
    )

    hhsearch_result = hhsearch_pdb70_runner.query(a3m_lines)
    hhsearch_hits = pipeline.parsers.parse_hhr(hhsearch_result)
    templates_result = template_featurizer.get_templates(
        query_sequence=query_sequence, hits=hhsearch_hits
    )
    return dict(templates_result.features)

colabfold.batch.mk_template = mk_template


import importlib_metadata
from pathlib import Path
import matplotlib.pyplot as plt

from sys import version_info
python_version = f"{version_info.major}.{version_info.minor}"

# Logging
logger = logging.getLogger(__name__)

def add_hash(x,y):
  return x+"_"+hashlib.sha1(y.encode()).hexdigest()[:5]

def input_features_callback(input_features):
  if display_images:
    plot_msa_v2(input_features)
    plt.show()
    plt.close()

def prediction_callback(protein_obj, length,
                        prediction_result, input_features, mode):
  model_name, relaxed = mode
  if not relaxed:
    if display_images:
      fig = plot_protein(protein_obj, Ls=length, dpi=150)
      plt.show()
      plt.close()

def run_all(val_dict):

    # Input Protein sequence:
    query_sequence = val_dict["seq"]
    jobname = val_dict["jobname"]
    num_relax = val_dict["numrelax"]
    template_mode = val_dict["template"]

    use_amber = num_relax > 0

    # check if alphafold params directory exists
    params_path = val_dict["params_path"]
    if os.path.isdir(params_path):
        logger.info(f"Found AlphaFold2 params at {params_path}")
        download_param = False
        params_path = Path(params_path)
    else:
        logger.info(f"Will download AlphaFold2 params to {params_path}")
        download_param = True
        params_path = Path(".")


    # remove whitespaces
    query_sequence = "".join(query_sequence.split())

    basejobname = "".join(jobname.split())
    basejobname = re.sub(r'\W+', '', basejobname)
    jobname = add_hash(basejobname, query_sequence)

    # check if directory with jobname exists
    def check(folder):
      if os.path.exists(folder):
        return False
      else:
        return True
    if not check(jobname):
      n = 0
      while not check(f"{jobname}_{n}"): n += 1
      jobname = f"{jobname}_{n}"

    # make directory to save results
    os.makedirs(jobname, exist_ok=True)

    # save queries
    queries_path = os.path.join(jobname, f"{jobname}.csv")
    with open(queries_path, "w") as text_file:
      text_file.write(f"id,sequence\n{jobname},{query_sequence}")

    if template_mode == "pdb100":
      use_templates = True
      custom_template_path = None
    elif template_mode == "custom":
      custom_template_path = os.path.join(jobname,f"template")
      os.makedirs(custom_template_path, exist_ok=True)
      uploaded = files.upload()
      use_templates = True
      for fn in uploaded.keys():
        os.rename(fn,os.path.join(custom_template_path,fn))
    else:
      custom_template_path = None
      use_templates = False

    logger.info(f"jobname:{jobname}")
    logger.info(f"sequence:{query_sequence}")
    logger.info(f"length:{len(query_sequence.replace(':',''))}")

    # MSA

    msa_mode = val_dict["msa_mode"]
    pair_mode = val_dict["pair_mode"]
    host_url = val_dict["host_url"]

    # decide which a3m to use

    # Check if MSA file exist already ???

    if "mmseqs2" in msa_mode:
      a3m_file = os.path.join(jobname,f"{jobname}.a3m")

    elif msa_mode == "custom":
      a3m_file = os.path.join(jobname,f"{jobname}.custom.a3m")
      if not os.path.isfile(a3m_file):
        custom_msa_dict = files.upload()
        custom_msa = list(custom_msa_dict.keys())[0]
        header = 0
        import fileinput
        for line in fileinput.FileInput(custom_msa,inplace=1):
          if line.startswith(">"):
             header = header + 1
          if not line.rstrip():
            continue
          if line.startswith(">") == False and header == 1:
             query_sequence = line.rstrip()
          logger.info(line, end='')

        os.rename(custom_msa, a3m_file)
        queries_path=a3m_file
        logger.info(f"moving {custom_msa} to {a3m_file}")

    else:
      a3m_file = os.path.join(jobname,f"{jobname}.single_sequence.a3m")
      with open(a3m_file, "w") as text_file:
        text_file.write(">1\n%s" % query_sequence)

    # Advanced settings

    #@markdown ### Advanced settings
    model_type = val_dict["model_type"]
    num_recycles = val_dict["num_recycles"]
    recycle_early_stop_tolerance = val_dict["recycle_early_stop_tolerance"]
    relax_max_iterations = val_dict["relax_max_iterations"]
    pairing_strategy = val_dict["pairing_strategy"]

    max_msa = val_dict["max_msa"]
    num_seeds = val_dict["num_seeds"]
    use_dropout = val_dict["use_dropout"]

    num_recycles = None if num_recycles == "auto" else int(num_recycles)
    recycle_early_stop_tolerance = None if recycle_early_stop_tolerance == "auto" else float(recycle_early_stop_tolerance)
    if max_msa == "auto": max_msa = None

    #@markdown #### Save settings
    save_all = val_dict["save_all"]
    save_recycles = val_dict["save_recycles"]
    dpi = val_dict["dpi"] 

    global display_images
    display_images = val_dict["images"] 

    result_dir = jobname
    log_filename = os.path.join(jobname,"log.txt")
    setup_logging(Path(log_filename))

    queries, is_complex = get_queries(queries_path)
    model_type = set_model_type(is_complex, model_type)

    if "multimer" in model_type and max_msa is not None:
      use_cluster_profile = False
    else:
      use_cluster_profile = True

    version = importlib_metadata.version("colabfold")
    logger.info(f"colabfold/{version}")

    if download_param:
        download_alphafold_params(model_type, params_path)

    results = run(
        queries=queries,
        result_dir=result_dir,
        use_templates=use_templates,
        custom_template_path=custom_template_path,
        num_relax=num_relax,
        msa_mode=msa_mode,
        model_type=model_type,
        num_models=5,
        num_recycles=num_recycles,
        relax_max_iterations=relax_max_iterations,
        recycle_early_stop_tolerance=recycle_early_stop_tolerance,
        num_seeds=num_seeds,
        use_dropout=use_dropout,
        model_order=[1,2,3,4,5],
        is_complex=is_complex,
        data_dir=Path(params_path),
        host_url=host_url,
        keep_existing_results=False,
        rank_by="auto",
        pair_mode=pair_mode,
        pairing_strategy=pairing_strategy,
        stop_at_score=float(100),
        prediction_callback=prediction_callback,
        dpi=dpi,
        zip_results=False,
        save_all=save_all,
        max_msa=max_msa,
        use_cluster_profile=use_cluster_profile,
        input_features_callback=input_features_callback,
        save_recycles=save_recycles,
        user_agent=f"colabfold/{version}",
    )

    results["out_zip"] = f"{result_dir}.zip"
    results["dir"] = result_dir
    results["is_complex"] = is_complex
    results["queries"] = queries

    shutil.make_archive(result_dir, 'zip', ".", result_dir)
    logger.info(f"Job finished successfully! Check results in {result_dir}.zip")

    return results
