#!/usr/bin/env python3
# coding: utf-8

import logging
import os
import sys

# Autorship information
__author__ = "Samuel Murail, Julien Rey"
__copyright__ = "Copyright 2024, RPBS"
__credits__ = ["Samuel Murail", "Julien Rey"]
__license__ = "GNU General Public License v2.0"
__version__ = "0.0.1"
__maintainer__ = "Samuel Murail, Julien Rey"
__email__ = "samuel.murail@u-paris.fr, julien.rey@u-paris.fr"
__status__ = "Beta"

# Logging
logger = logging.getLogger(__name__)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def hide_log():
    logger.setLevel(logging.WARNING)

