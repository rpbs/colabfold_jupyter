import ipywidgets as widgets
from IPython.display import FileLink
import os
import logging
from colabfold_jupyter import launch

# Logging
logger = logging.getLogger(__name__)

class DownloadFileLink(FileLink):
    # This class is to look like a button
    html_link_str = "<a class='lm-Widget jupyter-widgets jupyter-button widget-button mod-primary'" \
    "style='color: white;' href='{link}' download={file_name}>{link_text}</a>"

    def __init__(self, path, file_name=None, link_text=None, *args, **kwargs):
        super(DownloadFileLink, self).__init__(path, *args, **kwargs)

        self.file_name = file_name or os.path.split(path)[1]
        self.link_text = link_text or self.file_name

    def _format_path(self):
        from html import escape

        fp = "".join([self.url_prefix, escape(self.path)])
        return "".join(
            [
                self.result_html_prefix,
                self.html_link_str.format(
                    link=fp,
                    file_name=self.file_name,
                    link_text=self.link_text
                ),
                self.result_html_suffix,
            ]
        )

# Input widgets
seq_widget = widgets.Textarea(
    value='MASEQENCE',
    placeholder='Type something',
    description='query_sequence:',
    disabled=False,
    layout=widgets.Layout(height="100px", width="auto", paddingleft="100px")
)
jobname_widget = widgets.Text(
    value='Test',
    placeholder='Type something',
    description='jobname:',
    disabled=False   
)
numrelax_widget = widgets.Dropdown(
    options=[0, 1, 5],
    value=0,
    description='num_relax:',
    disabled=False,
)
template_widget = widgets.Dropdown(
    options=["none", "pdb100","custom"],
    value="none",
    description='template_mode:',
    disabled=False,
)

# MSA
msa_mode_widget = widgets.Dropdown(
    options=["mmseqs2_uniref_env", "mmseqs2_uniref","single_sequence","custom"],
    value="mmseqs2_uniref_env",
    description='msa_mode:',
    disabled=False,
)
pair_mode_widget = widgets.Dropdown(
    options=["unpaired_paired","paired","unpaired"],
    value="unpaired_paired",
    description='pair_mode:',
    disabled=False,
)

# Advanced settings
model_type_widget = widgets.Dropdown(
    options=["auto", "alphafold2_ptm", "alphafold2_multimer_v1", "alphafold2_multimer_v2", "alphafold2_multimer_v3", "deepfold_v1"],
    value="auto",
    description='model_type:',
    disabled=False,
)
num_recycles_widget = widgets.Dropdown(
    options=["auto", "0", "1", "3", "6", "12", "24", "48"],
    value="3",
    description='num_recycles:',
    disabled=False,
)
recycle_early_stop_tolerance = widgets.Dropdown(
    options=["auto", "0.0", "0.5", "1.0"],
    value="auto",
    description='recycle_early_stop_tolerance:',
    disabled=False,
)
relax_max_iterations_widget = widgets.Dropdown(
    options=[0, 200, 2000],
    value=200,
    description='relax_max_iterations:',
    disabled=False,
)
pairing_strategy_widget = widgets.Dropdown(
    options=["greedy", "complete"],
    value="greedy",
    description='pairing_strategy:',
    disabled=False,
)
max_msa_widget = widgets.Dropdown(
    options=["auto", "512:1024", "256:512", "64:128", "32:64", "16:32"],
    value="auto",
    description='max_msa:',
    disabled=False,
)
num_seeds_widget = widgets.Dropdown(
    options=[1,2,4,8,16],
    value=1,
    description='num_seeds:',
    disabled=False,
)
use_dropout_widget = widgets.Checkbox(
    value=False,
    description='use_dropout',
    disabled=False,
    indent=False
)

# Save settings
save_all_widget = widgets.Checkbox(
    value=False,
    description='save_all',
    disabled=False,
    indent=False
)
save_recycles_widget = widgets.Checkbox(
    value=False,
    description='save_recycles',
    disabled=False,
    indent=False
)
dpi_widget = widgets.BoundedIntText(
    value=200,
    min=50,
    max=400,
    step=1,
    description='dpi:',
    disabled=False,
)
images_widget = widgets.Checkbox(
    value=True,
    description='display_images',
    disabled=False,
    indent=False
)

# Server parameters
alphafold_parameters_widget = widgets.Text(
    value='/shared/banks/alphafold2/current/',
    placeholder='Type something',
    description='alphafold_parameters_path:',
    disabled=True
)
host_url_widget = widgets.Text(
    value='http://cpu-node146:3000',
    placeholder='Type something',
    description='host_url:',
    disabled=True
)

# Set description minimal widdht:
style_widgets = widgets.HTML("""
<style>
.jupyter-widgets label { font-weight: bold }
.widget-label { min-width: 30ex }
.widget-inline-hbox .widget-label { text-align: left }
:root { --jp-widgets-inline-width: }
h1 { font-size: 1.5em; text-align: center; }
.title_img { display: block; margin:auto; text-align:center;}
img { height: 100px; margin: 10px;}
</style>
""")

# description widgets:

Header_widgets = widgets.HTML(
    value='''
<h1>AlphaFold 2 by RPBS</h1>
<br>
<div class="title_img">
    <img alt="RPBS" src="https://rpbs.docs.rpbs.univ-paris-diderot.fr/documentation/imgs/rpbs.png">
    <img alt="UPC" src="https://u-paris.fr/wp-content/uploads/2022/03/Universite_Paris-Cite-logo.jpeg">
</div>
''',
)

input_prot_widgets = widgets.HTML(
    value="<h2>Input protein sequence(s)</h2>",
)
seq_descr_widgets = widgets.HTML(
    value="<li>Use <strong>:</strong> to specify inter-protein chainbreaks for <strong>modeling complexes</strong> (supports homo- and hetro-oligomers). For example <strong>PI...SK:PI...SK</strong> for a homodimer.</li>"
)
numrelax_descr_widgets = widgets.HTML(
    value="<li>Specify how many of the top ranked structures to relax using amber.</li>"
)
template_descr_widgets = widgets.HTML(
    value="<li><code>none</code> = no template information is used. <code>pdb100</code> = detect templates in pdb100 (see <a href='#pdb100'>notes</a>). <code>custom</code> - upload and search own templates (PDB or mmCIF format, see <a href='#custom_templates'>notes</a>).</li>"
)
msa_options_widgets = widgets.HTML(
    value="<h2>MSA options (custom MSA upload, single sequence, pairing mode).</h2>"
)
pair_descr_widgets = widgets.HTML(
    value="<li>\"unpaired_paired\" = pair sequences from same species + unpaired MSA, \"unpaired\" = seperate MSA for each chain, \"paired\" - only use paired sequences.</li>"
)

advanced_descr_widgets = widgets.HTML(
    value="<h2>Advanced settings</h2>"
)

model_descr_widgets = widgets.HTML(
    value="""<li>If <code>auto</code> selected, will use <code>alphafold2_ptm</code> for monomer prediction and <code>alphafold2_multimer_v3</code> for complex prediction.
Any of the mode_types can be used (regardless if input is monomer or complex).</li>"""
)

num_recycles_descr_widgets = widgets.HTML(
    value="<li>If <code>auto</code> selected, will use <code>num_recycles=20</code> if <code>model_type=alphafold2_multimer_v3</code>, else <code>num_recycles=3</code>.</li>"
)
recycle_early_stop_tolerance_descr_widgets = widgets.HTML(
    value="<li>If <code>auto</code> selected, will use <code>tol=0.5</code> if <code>model_type=alphafold2_multimer_v3</code> else <code>tol=0.0</code>.</li>"
)
relax_max_descr_widgets = widgets.HTML(
    value="<li>Max amber relax iterations, <code>0</code> = unlimited (AlphaFold2 default, can take very long).</li>"
)
greedy_descr_widgets = widgets.HTML(
    value="<li><code>greedy</code> = pair any taxonomically matching subsets, <code>complete</code> = all sequences have to match in one line.</li>"

)
sample_settings_widgets = widgets.HTML(
    value="""<h3>Sample settings</h3>
    <li>Enable dropouts and increase number of seeds to sample predictions from uncertainty of the model.</li>
    <li>Decrease <code>max_msa</code> to increase uncertainity.</li>
    """
)
save_descr_widgets = widgets.HTML(
    value="<h3>Save settings</h4>"
)
dpi_descr_widgets = widgets.HTML(
    value="<li>Set dpi for image resolution.</li>"
)

server_parameters_widgets = widgets.HTML(
    value="<h3>Server params</h4>"
)

launch_button = widgets.Button(description="Launch Colabfold", button_style='primary')
output_widget = widgets.Output()

def on_button_clicked(b):

    global results
    output_widget.clear_output()

    with output_widget:
        logger.info("Launch Colabfold.")
        val_dict = get_widget_value()
        results = launch.run_all(val_dict)
        out_zip = results["out_zip"]
        results.update(val_dict)

        logger.info(results)

        download_button = DownloadFileLink(out_zip, out_zip, "Download")

        display(download_button)

launch_button.on_click(on_button_clicked)


def show_widgets():
    display(Header_widgets)
    display(style_widgets)
    display(input_prot_widgets)
    display(seq_widget)
    display(seq_descr_widgets)
    display(jobname_widget)
    display(numrelax_widget)
    display(numrelax_descr_widgets)
    display(template_widget)
    display(template_descr_widgets)

    display(msa_options_widgets)
    display(msa_mode_widget)
    display(pair_mode_widget)
    display(pair_descr_widgets)
    
    display(advanced_descr_widgets)
    display(model_type_widget)
    display(model_descr_widgets)
    display(num_recycles_widget)
    display(num_recycles_descr_widgets)
    display(recycle_early_stop_tolerance)
    display(recycle_early_stop_tolerance_descr_widgets)
    display(relax_max_iterations_widget)
    display(relax_max_descr_widgets)
    display(pairing_strategy_widget)
    display(greedy_descr_widgets)

    display(sample_settings_widgets)
    display(max_msa_widget)
    display(num_seeds_widget)
    display(use_dropout_widget)
    display(save_descr_widgets)
    
    display(save_all_widget)
    display(save_recycles_widget)
    display(dpi_widget)
    display(dpi_descr_widgets)
    display(images_widget)

    display(server_parameters_widgets)
    display(alphafold_parameters_widget)
    display(host_url_widget)

    display(launch_button)

    display(output_widget)

def get_widget_value():

    val_dict = {
        'seq': seq_widget.value,
        'jobname': jobname_widget.value,
        'numrelax': numrelax_widget.value,
        'template': template_widget.value,
        'msa_mode': msa_mode_widget.value,
        'pair_mode': pair_mode_widget.value,
        'model_type': model_type_widget.value,
        'num_recycles': num_recycles_widget.value,
        'recycle_early_stop_tolerance': recycle_early_stop_tolerance.value,
        'relax_max_iterations': relax_max_iterations_widget.value,
        'pairing_strategy': pairing_strategy_widget.value,
        'max_msa': max_msa_widget.value,
        'num_seeds': num_seeds_widget.value,
        'use_dropout': use_dropout_widget.value,
        'save_all': save_all_widget.value,
        'save_recycles': save_recycles_widget.value,
        'dpi': dpi_widget.value,
        'images': images_widget.value,
        'params_path': alphafold_parameters_widget.value,
        'host_url': host_url_widget.value,
    }

    return val_dict

def show_pdb_best(results):

    import py3Dmol
    import glob
    import matplotlib.pyplot as plt
    from colabfold.colabfold import plot_plddt_legend
    from colabfold.colabfold import pymol_color_list, alphabet_list

    display(widgets.HTML("<h3>Display 3D structure</h3>"))

    rank_model_widgets = widgets.BoundedIntText(
        value=1,
        min=1,
        max=len(results["rank"][0]),
        step=1,
        description='rank model:',
        disabled=False
    )
    display(rank_model_widgets)

    color_widget = widgets.Dropdown(
        options=["chain", "lDDT", "rainbow"],
        value="chain",
        description='color:',
        disabled=False,
    )
    display(color_widget)

    sidechains_widget = widgets.Checkbox(
        value=False,
        description='Show sidechains:',
        disabled=False,
        indent=False
    )
    display(sidechains_widget)

    mainchains_widget = widgets.Checkbox(
        value=False,
        description='Show mainchains:',
        disabled=False,
        indent=False
    )
    display(mainchains_widget)

    rank_num = rank_model_widgets.value
    tag = results["rank"][0][rank_num - 1]
    jobname_prefix = ".custom" if results['msa_mode'] == "custom" else ""
    pdb_filename = f"{results['dir']}/{results['dir']}{jobname_prefix}_unrelaxed_{tag}.pdb"
    pdb_file = glob.glob(pdb_filename)

    def show_pdb(rank_num=1, show_sidechains=False, show_mainchains=False, color="lDDT"):

        tag = results["rank"][0][rank_num - 1]
        pdb_filename = f"{results['dir']}/{results['dir']}{jobname_prefix}_unrelaxed_{tag}.pdb"
        pdb_file = glob.glob(pdb_filename)

        view = py3Dmol.view(js='https://3dmol.org/build/3Dmol.js',)
        view.addModel(open(pdb_file[0],'r').read(),'pdb')

        if color == "lDDT":
            view.setStyle({'cartoon': {'colorscheme': {'prop':'b','gradient': 'roygb','min':50,'max':90}}})
        elif color == "rainbow":
            view.setStyle({'cartoon': {'color':'spectrum'}})
        elif color == "chain":
            chains = len(results["queries"][0][1]) + 1 if results["is_complex"] else 1
            for n,chain,color in zip(range(chains),alphabet_list,pymol_color_list):
                view.setStyle({'chain':chain},{'cartoon': {'color':color}})

        if show_sidechains:
            BB = ['C','O','N']
            view.addStyle({'and':[{'resn':["GLY","PRO"],'invert':True},{'atom':BB,'invert':True}]},
                                {'stick':{'colorscheme':f"WhiteCarbon",'radius':0.3}})
            view.addStyle({'and':[{'resn':"GLY"},{'atom':'CA'}]},
                                {'sphere':{'colorscheme':f"WhiteCarbon",'radius':0.3}})
            view.addStyle({'and':[{'resn':"PRO"},{'atom':['C','O'],'invert':True}]},
                                {'stick':{'colorscheme':f"WhiteCarbon",'radius':0.3}})
        if show_mainchains:
            BB = ['C','O','N','CA']
            view.addStyle({'atom':BB},{'stick':{'colorscheme':f"WhiteCarbon",'radius':0.3}})

        view.zoomTo()
        return view
    
    output2 = widgets.Output()
    display(output2)
    with output2:
        show_pdb(rank_num, sidechains_widget.value, mainchains_widget.value, color_widget.value).show()
        logger.info(results['metric'][0][rank_num - 1]['print_line'])

    #show_pdb(rank_num, sidechains_widget.value, mainchains_widget.value, color_widget.value).show()
    if color_widget.value == "lDDT":
        plot_plddt_legend().show()

    def on_value_change(change):
        output2.clear_output()
        with output2:
            show_pdb(rank_model_widgets.value - 1, sidechains_widget.value, mainchains_widget.value, color_widget.value).show()
            if color_widget.value == "lDDT":
                plot_plddt_legend().show()
            logger.info(results['metric'][0][rank_model_widgets.value - 1]['print_line'])

    rank_model_widgets.observe(on_value_change, names='value')
    color_widget.observe(on_value_change, names='value')
    sidechains_widget.observe(on_value_change, names='value')
    mainchains_widget.observe(on_value_change, names='value')

show_widgets()
