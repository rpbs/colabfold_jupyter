About Colabfold-Jupyter
===============

``colabfold_jupyter`` is a python library designed to facilitate using
colabfold in jupyter notebook. 


* Source code repository:
   https://gitlab.rpbs.univ-paris-diderot.fr/rpbs/colabfold_jupyter


Installation
------------

``colabfold_jupyter`` can be installed throught ``conda``/``mamba`` and ``pip``:

.. code-block:: bash
    COLABFOLD_VERSION="1.5.5"
    # Create a new conda environment:
    mamba create -y -n colabfold_A100 -c conda-forge -c bioconda -c "nvidia/label/cuda-11.8.0" \
    colabfold=$COLABFOLD_VERSION jaxlib=*=cuda* cuda-nvcc
    conda activate colabfold_A100
    # Install colabfold_jupyter:
    pip install git+https://gitlab.rpbs.univ-paris-diderot.fr/rpbs/colabfold_jupyter.git
    # Install af2_analysis:
    pip install git+https://github.com/samuelmurail/af2_analysis.git

``colabfold_jupyter`` source code is available on `gitlab.rpbs.univ-paris-diderot.fr``:

.. code-block:: bash

    COLABFOLD_VERSION="1.5.5"
    # Create a new conda environment:
    mamba create -y -n colabfold_A100 -c conda-forge -c bioconda -c "nvidia/label/cuda-11.8.0" \
    colabfold=$COLABFOLD_VERSION jaxlib=*=cuda* cuda-nvcc
    conda activate colabfold_A100
    # Install colabfold_jupyter:
    git clone https://gitlab.rpbs.univ-paris-diderot.fr/rpbs/colabfold_jupyter
    cd colabfold_jupyter
    python setup.py install
    # Install colabfold_jupyter as a jupyter kernel:
    python -m ipykernel install --user --name 'colabfold_A100' --display-name "colabfold_1.5.5_A100" 


Author
--------------

* `Samuel Murail <https://samuelmurail.github.io/PersonalPage/>`_, Associate Professor - `Université Paris Cité <https://u-paris.fr>`_,
`CMPLI <http://bfa.univ-paris-diderot.fr/equipe-8/>`_.

* Julien Rey, Research Ingenior - `Université Paris Cité <https://u-paris.fr>`_,
`CMPLI <http://bfa.univ-paris-diderot.fr/equipe-8/>`_.

See also the list of `contributors <https://gitlab.rpbs.univ-paris-diderot.fr/rpbs/colabfold_jupyter/-/graphs/main>`_ 
who participated in this project.

License
--------------

This project is licensed under the GNU General Public License v2.0 - see the ``LICENSE`` file for details.
